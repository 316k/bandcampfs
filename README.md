# BandcampFS : bandcamp in your `$HOME`

FUSE filesystem to access music from Bandcamp.

This gives a simple and convenient interface for all your programs :

```bash
$ # List all albums by artist CRABE
$ ls ~/bandcamp/crabe
$ # List tracks
$ ls ~/bandcamp/crabe/anti-vague
$ # Copy a song on your local disk
$ cp ~/bandcamp/crabe/anti-vague/01-* /tmp/test.mp3
$ # Stream it in any media player or software you want
$ vlc ~/bandcamp/crabe/anti-vague/*.mp3
$ lame --decode ~/bandcamp/crabe/le-temps-f33l/01-*.mp3 /tmp/song.wav
$ # Pitch-shift + add reverb on a song while streaming it directly from bandcamp
$ lame --decode \
  ~/bandcamp/crabe/vraiment-ma-pointe-de-pizza/01-Vraiment\ ma\ pointe\ de\ PIZZA.mp3 - |
  play - pitch 1000 reverb -w
```

This program was initialily designed to stream songs from bandcamp
using [moc](http://moc.daper.net/).

*Note: don't confuse this program with [the other
bandcampfs](https://github.com/tuxxy/BandcampFS), which is not an
actual FS but rather a very funny proof of concept.*

## Usage

Mount the filesystem somewhere (eg. in your home) with :

```bash
$ ./bandcampfs.py ~/bandcamp/
```

Add an artist by accessing to its directory :

```bash
$ ls ~/bandcamp/amandapalmer/
```

You should then see it appear in `~/bandcamp/'

```bash
$ ls ~/bandcamp/
amandapalmer
```

When you're done, unmount the filesystem with :

```bash
$ fusermount -u ~/bandcamp
```

## Installation

Install the dev files for FUSE :

    # apt install libfuse-dev

Or on dnf-based systems :

    # dnf install fuse-devel

Then, install the python dependencies :

```
$ pip install --user -r requirements.txt
```

## TODO

- [x] List individual tracks (tracks that aren't attached to an album)
- [x] Cache management
    - [x] rmdir artist/ to remove it from cache
    - [x] remove albums as well when an artist is deleted
    - [x] rmdir artist/album/ to remove all the tracks from cache
    - [x] rm artist/album/track.mp3 to remove it from cache
- [x] ~Offline support?~
    - Buy the album you want and download it
- [ ] Prettier folder names such as /My Band/2015 - My Album/ (while
      still supporting the /artist-id/album-slug/ syntax)
- [-] {create,access,modified}time on dirs/files
    - [x] Tracks upload time as file time (e.g. `ls -l
          /artist/album/track.mp3`)
    - [ ] Album released as directory modified time (e.g. `ls -l
          /artist/album/`)
- [x] Album infos in /band/album/infos.txt
- [x] Album thumbnail in `/artist/album/cover.jpg`
- [ ] Track lyrics in a file for moc (.lyricsText in /track/...)
- [ ] Stream higher quality than mp3 when possible
    - Content-Length is not defined for other formats, find a
      workaround
- [ ] Fix albums list when `/music` is disabled
- [x] Speedup dir listing
    - Actual URLs of the streams can't be cached for too long (there's a
      timestamp and a token in the URL, and a `HTTP/1.1 410 Gone` is
      received for old URLs
    - This is handled with expiry time on url_infos cache
    - url_infos is called too often, even when the stream url isn't
      required (ex.: listing a directory, simply to get the file size)
- [ ] Add resilience to lost connexion (eg. computer is offline when a
      track is fetched)
    - pip install func-timeout ?
- [ ] More efficient PickledDict (stop rewriting everything all the
      time)
- [x] Fix missing data in album/infos.txt
- [ ] Fix track/infos.txt

## Bugs

This program is still experimental but works pretty fine most of the
time.

If you encounter unsupported cases, let me know!
