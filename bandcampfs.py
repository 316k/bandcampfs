#!/usr/bin/python3

import os, stat, errno
import re
import subprocess
from random import random
from collections import namedtuple
from functools import lru_cache
import html

import fuse
from fuse import Fuse
import youtube_dl
import requests
from pyquery import PyQuery as pyq
from dateutil.parser import parse as parse_date

from filecache import file_cache, FileSet, PickledDict, set_logger as filecache_set_logger

# In seconds
MINUTES = 60
HOURS = 60 * MINUTES
DAYS = 24 * HOURS

basepath = os.path.dirname(os.path.abspath(__file__))

ydl_opts = {
    'key': 'FFmpegExtractAudio',
    'simulate': True,
    'quiet': True,
    'verbose': False,
    'restrictfilenames': True,
}

def logit(*args):
    msg = ' '.join(map(str, args))
    with open(basepath + '/bandcampfs.log', 'a') as f:
        f.write(msg +'\n')

filecache_set_logger(logit)

if not hasattr(fuse, '__version__'):
    raise RuntimeError("your fuse-py doesn't know of fuse.__version__, probably it's too old.")

with open(basepath + '/bandcampfs.log', 'w') as f:
    f.write('')

fuse.fuse_python_api = (0, 2)

# PickledDict: (artist, album) => [(id, url_track1), (id, url_track2), ...]
# URLs are ...bandcamp.com/track/track-name
track_infos = PickledDict(basepath + '/track-infos.cache')

# https://artist.bandcamp.com/track/... => (ext, size, ts, original url (not stream url))
TrackInfos = namedtuple('TrackInfos', ('ext', 'size', 'timestamp', 'src_url'))

# All known artists
artists = FileSet(basepath + '/all-artists.cache')


@file_cache(max_age=30 * MINUTES)
def youtube_dl_info(url):
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        return ydl.extract_info(url)

@file_cache(max_age=30 * MINUTES)
def stream_url(track_url):
    logit('fetching stream url for ', track_url)
    infos = youtube_dl_info(track_url)

    fmt = infos
    orig = True

    if infos['format_id'] != 'mp3-128':
        orig = False
        fmt = [
            f for f in infos['formats']
            if f['format_id'] == 'mp3-128'
        ][0]

    return fmt['url']

@file_cache(basepath + '/url-header-infos.cache')
def url_header_infos(url):
    h = requests.head(url)

    size = int(h.headers['Content-Length'])
    date = int(parse_date(h.headers['Last-Modified']).timestamp())

    ret = TrackInfos(url.split('.')[-1], int(size), date, url)

    return ret

@file_cache(basepath + '/file-infos.cache')
def url_infos(url):
    logit('fetch:', url)

    # Fetch the url for streaming the sound file
    infos = youtube_dl_info(url)

    fmt = infos

    if infos['format_id'] != 'mp3-128':
        fmt = [
            f for f in infos['formats']
            if f['format_id'] == 'mp3-128'
        ][0]

    stream = stream_url(url)

    h = requests.head(stream)
    size = int(h.headers['Content-Length'])
    ret = TrackInfos(fmt['ext'], int(size), int(infos['timestamp']), url)
    return ret

@file_cache(basepath + '/album-infos.cache', max_age=7 * DAYS)
def album_infos(artist, album):
    page = pyq(url='https://' + artist + '.bandcamp.com/album/' + album, encoding='utf-8')
    return page('#trackInfo .tralbumData').text() + '\n'

def track_url(artist, album, tracknum):
    ls_album(artist, album) # Refresh album infos
    return track_infos[(artist, album)][tracknum]

def url_infos_from_path(path):
    return url_infos(track_url(*pathinfo_track(path)))

@file_cache(basepath + '/artists.cache', max_age=7 * DAYS)
def ls_artist(artist):
    """Returns all albums by a given artist"""
    artists.add(artist)

    slugs = set()

    # FIXME : While /music is used in most places, some bandcamp pages
    # disable it (eg. https://jeanleloup.bandcamp.com/music)
    #
    # In those cases, the latest album's tracks are shown along with
    # the albums list
    page = pyq(url='https://' + artist + '.bandcamp.com/music', encoding='utf-8')

    for title in page('a'):

        if 'href' not in title.attrib:
            continue

        url = title.attrib['href']

        if not re.search(r'(^/album/|^/track/)', url) or '#' in url or '?action' in url:
            continue

        if url not in slugs:
            slugs.add(url)

    ret = [re.sub(r'^(/album/|/track/)', '', url) for url in slugs]
    return ret

@file_cache(basepath + '/albums.cache')
def ls_album(artist, album):
    """Return all tracks"""
    artists.add(artist)
    single_track = False

    names = []
    urls = []

    album_url = 'https://' + artist + '.bandcamp.com/album/' + album

    # Find thumbnail
    r = requests.get(album_url)

    if r.status_code == 404:
        # Single track instead
        album_url = 'https://' + artist + '.bandcamp.com/track/' + album
        r = requests.get(album_url)
        single_track = True

    page = pyq(r.content)
    image = page('#tralbumArt img')[0]

    thumbnail = ""

    try:
        thumbnail = image.attrib['src']
    except Exception as e:
        logit('err', e)

    def parse_track(i, entry, track_url_infos):
        ext = track_url_infos.ext

        name = '{:>02}'.format(i + 1) + '-' + entry['title'] + '.' + ext
        clean_name = html.unescape(name).replace('/', '-')
        names.append(clean_name)

        urls.append(entry['url'])

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        infos = ydl.extract_info(album_url, process=False)

        if single_track:
            entry = {'title': infos['title'], 'url': album_url}
            parse_track(0, entry, url_infos(album_url))
        else:
            for i, entry in enumerate(infos['entries']):
                parse_track(i, entry, url_infos(entry['url']))

    track_infos[(artist, album)] = tuple(urls) + (thumbnail,)

    return tuple(names) + ('cover.jpg',)

def file_type(path):
    if path == '/':
        return 'root'

    if re.search(r'^/([^/._]+)/?$', path):
        return 'artist'

    if re.search(r'^/([^/._]+)/([^/._]+)/?$', path):
        return 'album'

    if path.endswith('/cover.jpg'):
        return 'cover'

    if path.endswith('/infos.txt'):
        return 'infos'

    if re.search(r'^/([^/._]+)/([^/._]+)/([0-9]{2,})-([^/]+)\.([a-zA-Z0-9]{2,4})$', path):
        return 'track'

    return False

def pathinfo_artist(path):
    return re.search(r'^/([^/._]+)/?$', path).group(1)

def pathinfo_album(path):
    return re.search(r'^/([^/._]+)/([^/._]+)/?$', path).groups()

def pathinfo_track(path):
    artist, album, track = re.search(r'^/([^/.]+)/([^/.]+)/([0-9]{2,})-([^/]+)\.([a-zA-Z0-9]{2,4})$', path).groups()[:3]
    return artist, album, int(track) - 1

def cover_url(path):
    artist, album = re.search(r'^/([^/.]+)/([^/.]+)/cover\.jpg$', path).groups()[:2]
    return track_infos[(artist, album)][-1]

class MyStat(fuse.Stat):
    def __init__(self):
        self.st_mode = 0
        self.st_ino = 0
        self.st_dev = 0
        self.st_nlink = 0
        self.st_uid = 0
        self.st_gid = 0
        self.st_size = 0
        self.st_atime = 0
        self.st_mtime = 0
        self.st_ctime = 0

    def settime(self, t):
        self.st_atime = t
        self.st_mtime = t
        self.st_ctime = t

class BandcampFS(Fuse):
    def getattr(self, path):
        st = MyStat()

        ftype = file_type(path)

        if ftype in ('root', 'artist', 'album'):
            # Directory
            st.st_mode = stat.S_IFDIR | 0o755
            st.st_nlink = 2
        elif ftype == 'track':
            # File
            st.st_mode = stat.S_IFREG | 0o444
            st.st_nlink = 1
            infos = url_infos_from_path(path)
            st.st_size = infos.size
            st.settime(infos.timestamp)
        elif ftype == 'cover':
            st.st_mode = stat.S_IFREG | 0o444
            st.st_nlink = 1
            infos = url_header_infos(cover_url(path))
            st.st_size = infos.size
            st.settime(infos.timestamp)
        elif ftype == 'infos':
            st.st_mode = stat.S_IFREG | 0o444
            st.st_nlink = 1
            artist, album = pathinfo_album(re.sub(r'/infos.txt$', '', path))
            content = album_infos(artist, album)
            st.st_size = len(content.encode('utf8'))
        else:
            return -errno.ENOENT

        return st

    def readdir(self, path, offset):
        yield fuse.Direntry('.')
        yield fuse.Direntry('..')

        ftype = file_type(path)

        if ftype == 'root':
            for artist in artists:
                yield fuse.Direntry(artist)
        elif ftype == 'artist':
            artist = pathinfo_artist(path)
            for album in ls_artist(artist):
                yield fuse.Direntry(album)
        elif ftype == 'album':
            artist, album = pathinfo_album(path)

            for track in ls_album(artist, album):
                yield fuse.Direntry(track)

            yield fuse.Direntry('infos.txt')

    def open(self, path, flags):
        accmode = os.O_RDONLY | os.O_WRONLY | os.O_RDWR
        if (flags & accmode) != os.O_RDONLY:
            return -errno.EACCES

    def unlink(self, path):
        ftype = file_type(path)
        if ftype != 'track':
            logit("can't delete that", path)
            return

        # For each track_infos
        artist, album, tracknum = pathinfo_track(path)

        url = track_url(artist, album, tracknum)
        url_infos.invalidate(url)

        ls_album.invalidate(artist, album)

        # TODO : delete only the requested song from the cache
        for url in track_infos[(artist, album)]:
            url_infos.invalidate(url)

        del track_infos[(artist, album)]

    def rmdir(self, path):
        if file_type(path) == 'artist':
            artist = pathinfo_artist(path)
            artists.remove(artist)

            # Remove albums from artist
            for bundle in ls_artist.cached_params():
                if bundle[0][0] == artist:
                    ls_artist.invalidate_bundle(bundle)

            # Delete tracks
            for bundle in ls_album.cached_params():
                k = bundle[0]
                if k[0] == artist:
                    # Delete track list
                    ls_album.invalidate_bundle(bundle)

                    # Delete all url_infos/track_infos
                    try:
                        for url in track_infos[k]:
                            url_infos.invalidate(url)

                        del track_infos[k]
                    except:
                        pass
        elif file_type(path) == 'album':
            artist, album = pathinfo_album(path)

            ls_album.invalidate(artist, album)

            # For each track_infos
            keys = list(track_infos.keys())
            for k in keys:
                if k == (artist, album):
                    # Delete url_infos
                    for url in track_infos[k]:
                        url_infos.invalidate(url)
                    # Remove from track_infos
                    del track_infos[k]
        else:
            logit("can't rmdir'", path)

    def read(self, path, size, offset):
        ftype = file_type(path)
        if ftype == 'track':
            logit('rd', path, size, offset)
            infos = url_infos_from_path(path)
            url = stream_url(infos.src_url)
        elif ftype == 'cover':
            infos = url_header_infos(cover_url(path))
            url = infos.src_url
        elif ftype == 'infos':
            artist, album = pathinfo_album(re.sub(r'/infos.txt$', '', path))
            content = album_infos(artist, album)
            return content.encode()[offset:offset + size]
        else:
            return b""

        filesize = infos.size
        if offset + size >= filesize:
            logit('read:', 'Asking for too many bytes...')
            # size = filesize - offset - 1

        headers = {"Range": "bytes={}-{}".format(offset, (offset + size))}

        r = requests.get(url, headers=headers)
        out = r.content

        return out[:size]

def tests():
    assert file_type('/millepiastresplease/les-cit-s-dor/01-test.mp3') == 'track'
    assert file_type('/millepiastresplease/les-cit-s-dor/10-abc.flac') == 'track'
    assert file_type('/millepiastresplease/les-cit-s-dor/198-Whatever the hell.ogg') == 'track'
    assert file_type('/millepiastresplease/les-cit-s-dor/') == 'album'
    assert file_type('/millepiastresplease/les-cit-s-dor') == 'album'
    assert file_type('/millepiastresplease/') == 'artist'
    assert file_type('/millepiastresplease') == 'artist'
    assert file_type('/') == 'root'
    assert file_type('/blub/blip/truc.txt') == False
    assert file_type('/blub/blip/no') == False
    assert file_type('/a/b/c/truc.mp3') == False

    assert pathinfo_track('/millepiastresplease/les-cit-s-dor/01-test.mp3') == ('millepiastresplease', 'les-cit-s-dor', 0)
    assert pathinfo_track('/glu/gla/12-test.mp3') == ('glu', 'gla', 11)

    assert pathinfo_track('/millepiastresplease/les-cit-s-dor/01-test.mp3') == ('millepiastresplease', 'les-cit-s-dor', 0)


if __name__ == '__main__':
    usage="""
Userspace hello example
""" + Fuse.fusage

    server = BandcampFS(version="%prog " + fuse.__version__,
                     usage=usage)

    server.parse(errex=1)
    server.main()
