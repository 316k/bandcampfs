#!/usr/bin/python3

import pickle
import collections
from time import time

log = print

def set_logger(fct):
    global log
    log = fct

class PickledDict(collections.abc.MutableMapping):
    """A dictionary that saves everything to a file through pickle and
       reloads it on init"""

    def __init__(self, fname):
        self.store = dict()
        self.fname = fname

        try:
            with open(fname, 'rb') as cache_file:
                load = pickle.load(cache_file)
                while True:
                    key, val = load
                    self.store[key] = val
                    load = pickle.load(cache_file)
        except Exception as e:
            pass

    def _rewrite_cache(self):
        with open(self.fname, 'wb') as cache_file:
            for key, value in self.store.items():
                pickled = pickle.dumps((
                    key,
                    value
                ))
                cache_file.write(pickled)

    def __getitem__(self, key):
        return self.store[self.__keytransform__(key)]

    def __setitem__(self, key, value):
        self.store[self.__keytransform__(key)] = value
        self._rewrite_cache()

    def __delitem__(self, key):
        del self.store[self.__keytransform__(key)]
        self._rewrite_cache()

    def __iter__(self):
        return iter(self.store)

    def __len__(self):
        return len(self.store)

    def __keytransform__(self, key):
        return key

    def __repr__(self):
        return 'PickledDict({' + ', '.join((repr(x) + ': ' + repr(y) for x, y in self.store.items())) + '})'

def file_cache(fname=None, max_age=None):
    """Simple file-backed cache

       fname: file used to store the cached results
       max_age: maximum time to keep a cached value"""

    def decorator(f, *args, **kwargs):
        if fname is None:
            cache = dict() # Don't write to file
        else:
            cache = PickledDict(fname)

        def cached(*args, **kwargs):
            bundle = (args, frozenset(sorted(kwargs.items())))

            if bundle in cache:
                out, ts = cache[bundle]

                if max_age is None or ts > time() - max_age:
                    return out
                else:
                    log(f.__name__, bundle, 'timed out')

            out = f(*args, **kwargs)

            cache[bundle] = out, time()

            return out

        def invalidate_bundle(bundle):
            try:
                del cache[bundle]
            except:
                log('No such bundle:', bundle)
                pass

        def invalidate(*args, **kwargs):
            bundle = (args, frozenset(sorted(kwargs.items())))
            invalidate_bundle(bundle)

        def invalidate_all():
            cache.clear()

        def cached_params():
            return [key for key in cache.keys()]

        cached.invalidate_bundle = invalidate_bundle
        cached.invalidate = invalidate
        cached.invalidate_all = invalidate_all
        cached.cached_params = cached_params

        return cached

    return decorator

class FileSet():
    def __init__(self, cache_fname):
        self.data = set()
        self.cache_fname = cache_fname
        try:
            with open(cache_fname, 'r') as f:
                for line in f:
                    self.data.add(line.strip())
        except:
            pass

    def add(self, val):
        if val in self:
            return

        self.data.add(val)
        with open(self.cache_fname, 'a') as f:
            f.write(val + '\n')

    def remove(self, val):
        if val not in self:
            return

        self.data.remove(val)

        with open(self.cache_fname, 'w') as f:
            for val in self.data:
                f.write(val + '\n')

    def contains(self, val):
        return val in self.data

    def __iter__(self):
        return (i for i in self.data)

    def __str__(self):
        return '{' + ','.join(repr(i) for i in self.data) + '}'

def test_file_cache():
    computed = []
    @file_cache('/tmp/fib.cache')
    def fib(x):
        computed.append(x)
        if x < 2:
            return 1
        return (fib(x - 1) + fib(x - 2))

    fib.invalidate_all()
    assert fib(80) == 37889062373143906
    assert len(set(computed).intersection(set(range(81)))) == 81

    computed.clear()

    fib.invalidate(35)
    assert len(list(fib.cached_params())) == 80
    fib(35) == 14930352

    assert len(list(fib.cached_params())) == 81

    assert len(computed) == 1 and computed[0] == 35

    fib.invalidate_all()
    assert len(list(fib.cached_params())) == 0

    computed.clear()

    @file_cache('/tmp/funky_fun.cache')
    def funky_fun(*args, wat="test", no="hum"):
        computed.append(True)
        return len(args) + len(wat) + len(no)

    funky_fun.invalidate_all()

    assert len(computed) == 0
    funky_fun(1, 2, 3, 4, wat="aaaaaaaa", no="nnnnnnnnnn")
    assert len(computed) == 1
    computed.clear()
    funky_fun(1, 2, 3, 4, wat="aaaaaaaa", no="nnnnnnnnnn")
    assert len(computed) == 0
    funky_fun(1, 2, 3, 4, no="nnnnnnnnnn", wat="aaaaaaaa")
    assert len(computed) == 0


def test_pickleddict():
    d = PickledDict('/tmp/test.cache')
    d[(1,2,3)] = 'tupple'
    d['test'] = 'txt'

    assert (1,2,3) in d
    assert d['test'] == 'txt'

    assert len(d) == 2

    del d['test']
    assert 'test' not in d
    assert (1,2,3) in d

def test_fileset():
    vals = FileSet('/tmp/filecache-test')
    vals.add('abc')
    vals.add('blib')
    vals.add('abc')
    vals.add('abc')
    vals.add('xyz')
    vals.add('blub')

    print(vals)

    vals = None
    vals = FileSet('/tmp/filecache-test')

    print(vals)
    vals.remove('blib')
    print(vals)

    vals = None
    vals = FileSet('/tmp/filecache-test')
    print(vals)


if __name__ == "__main__":
    test_file_cache()
    test_pickleddict()
